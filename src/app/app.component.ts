import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Posts';
  listePost =[
    {
      title: "Mon premier post",
      content: "je suis très comtent de suivre les cours sur openclassroom",
      loveIts: 0,
      created_at: new Date()
    },
    {
      title: "Mon deuxième post",
      content: "j'en n'ai suivi plusieurs cours comme angular",
      loveIts: 0,
      created_at: new Date()
    },
    {
      title: "Encore un post",
      content: "je suis un fan de openclassroom car ils forment bien les jeunes",
      loveIts: 0,
      created_at: new Date()
    },
   ];
  constructor(){
   
  }
}
